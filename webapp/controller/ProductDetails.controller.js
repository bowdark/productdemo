sap.ui.define([
	"com/bowdark/products/controller/BaseController",
	"sap/m/MessageBox",
	"sap/m/MessageToast"
], function(BaseController, MessageBox, MessageToast) {
	"use strict";

	return BaseController.extend("com.bowdark.products.controller.ProductDetails", {
		onInit: function(oEvent) {
			// Register a listener to react to routing requests:
			this.getRouter().getRoute("product-details").attachMatched(this.onLoadProduct, this);
		},
		
		onLoadProduct: function(oEvent) {
			var args = oEvent.getParameter("arguments");
			var sKey = this.getView().getModel().createKey("/ProductSet", {
				"ProductID": args.ProductId
			});
			
			this.getView().bindElement({ path: sKey,
				                         parameters: {expand: 'ToSupplier'} });
		},
		
		onNavBack: function(oEvent) {
			this.getRouter().navTo("product-list");
		},
		
		onDeleteProduct: function(oEvent) {
			MessageBox.confirm(this.getI18NText("productDeletePrompt"), {
				onClose: function(sButton) {
					if (sButton === MessageBox.Action.OK) {
						this.removeProduct();
					}
				}.bind(this)
			});
		},
		
		removeProduct: function() {
			var sPath = this.getView().getElementBinding().getPath();
			this.getModel().remove(sPath, {
				success: function(oData, response) {
					MessageToast.show(this.getI18NText("productDeleteSuccess"));
					this.onNavBack();
					this.getEventBus().publish("product-list", "refresh");
				}.bind(this),
				
				error: function(oError) {
					MessageBox.error(this.getI18NText("productDeleteFailure"));
				}
			});
		}
	});
});