sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("com.bowdark.products.controller.BaseController", {
		getModel: function(sName) {
			if (this.getOwnerComponent()) {
				return this.getOwnerComponent().getModel(sName);
			} else if (this.getView()) {
				return this.getView().getModel(sName);
			} else {
				return undefined;
			}
		},

		getRouter: function() {
			return this.getOwnerComponent().getRouter();
		},

		getDeviceModel: function() {
			return this.getOwnerComponent().getModel("device").getProperty("/");
		},
		
		getI18NText: function(sKey) {
			var oModel = this.getOwnerComponent().getModel("i18n");
			return oModel.getProperty(sKey);
		},
		
		getEventBus: function() {
			return sap.ui.getCore().getEventBus();
		}
	});
});