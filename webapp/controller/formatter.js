sap.ui.define([], function() {
	"use strict";
	
	return {
		convertToDecimal: function(sValue) {
			return parseFloat(sValue);
		}
	};
});