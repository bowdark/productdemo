sap.ui.define([
	"com/bowdark/products/controller/BaseController",
	"com/bowdark/products/controller/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"sap/m/MessageToast"
], function(BaseController, formatter, Filter, FilterOperator, MessageBox, MessageToast) {
	"use strict";

	return BaseController.extend("com.bowdark.products.controller.ProductList", {
		formatter: formatter,
		
		onInit: function() {
			// Register a listener to respond to refresh events:
			this.getEventBus().subscribe("product-list", "refresh", this.reloadProductList, this);
		},
		
		onSelectProduct: function(oEvent) {
			// Process product selection requests:
			var oTable = oEvent.getSource();
			var oItem = oTable.getSelectedItem();

			this.getRouter().navTo("product-details", {
				ProductId: oItem.getBindingContext().getObject().ProductID
			});
		},

		onFilterProducts: function(oEvent) {
			// var sFilterValue = oEvent.getSource().getValue();
			// var oFilter = new Filter({
			// 	path: 'Name',
			// 	operator: FilterOperator.Contains,
			// 	value1: sFilterValue
			// });

			// var oTable = this.getView().byId("tabProducts");
			// oTable.getBinding("items").filter(oFilter);
		},
		
		reloadProductList: function() {
			var oTable = this.getView().byId("tabProducts");
			oTable.getBinding("items").refresh();
		},

		onOpenAddProduct: function(oEvent) {
			// Open up the Add Product dialog box:
			if (!this._oProductDialog) {
				this._oProductDialog = sap.ui.xmlfragment("com.bowdark.products.fragment.AddProduct", this);
				this.getView().addDependent(this._oProductDialog);
			}

			var oContext = this.getModel().createEntry('/ProductSet');
			oContext.getModel().setProperty(oContext.getPath() + "/CurrencyCode", "USD");
			this._oProductDialog.bindElement(oContext.getPath());
			
			this._oProductDialog.open();
		},
		
		onSupplierVH: function(oEvent) {
			// Open up the value help dialog box:
			if (!this.oSupplierVH) {
				this.oSupplierVH = sap.ui.xmlfragment("com.bowdark.products.fragment.SupplierLookup", this);
				this.getView().addDependent(this.oSupplierVH);
			}

			// Pre-filter the dialog box and open it:
			var sInputValue = oEvent.getSource().getValue();
			this.oSupplierVH.getBinding("items").filter([
				new Filter("CompanyName", FilterOperator.Contains, sInputValue)
			]);

			this.oSupplierVH.open(sInputValue);
		},
		
		onFilterSuppliers:  function(oEvent) {
			// Filter the supplier list by company name:
			var sValue = oEvent.getParameter("value");
			
			var oFilter = new Filter(
				"CompanyName",
				sap.ui.model.FilterOperator.Contains, sValue
			);
			
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},
		
		onCloseSupplierVH: function(oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			if (oSelectedItem) {
				var oSupplier = oSelectedItem.getBindingContext().getObject();
				
				var oSupplierId = sap.ui.getCore().byId("inpSupplierId");
				if (oSupplierId) {
					oSupplierId.setValue(oSupplier.BusinessPartnerID);
				}
				
				var oSupplierName = sap.ui.getCore().byId("inpSupplierName");
				if (oSupplierName) {
					oSupplierName.setValue(oSupplier.CompanyName);
				}
			}
			
			oEvent.getSource().getBinding("items").filter([]);
		},
		
		onCancelAddProduct: function(oEvent) {
			// Remove the staged Product entity from the queue:
			var oBinding = this._oProductDialog.getElementBinding();
			var oContext = oBinding.getBoundContext();
			this.getModel().deleteCreatedEntry(oContext);
			
			// Close the Add Product dialog box:
			this.closeAddProduct();
		},
		
		closeAddProduct: function() {
			// Close the Add Product dialog box:
			if (this._oProductDialog) {
				this._oProductDialog.close();
				this.getView().removeDependent(this._oProductDialog);
				this._oProductDialog.destroy();
				this._oProductDialog = null;
			}
		},
		
		onAddProduct: function(oEvent) {
			// Try to create the new product on the backend:
			this.getModel().submitChanges({
				success: function(oData) {
					MessageToast.show(this.getI18NText("productSuccess"));
					this.closeAddProduct();
					this.reloadProductList();
				}.bind(this),
				
				error: function(oError) {
					MessageBox.show(this.getI18NText("productFailure"));
					jQuery.sap.log.error(oError);
				}.bind(this)
			});
		}
	});
});