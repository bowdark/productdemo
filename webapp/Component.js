sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/bowdark/products/model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("com.bowdark.products.Component", {
		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// Call the base component's init function:
			UIComponent.prototype.init.apply(this, arguments);

			// Set the device model:
			this.setModel(models.createDeviceModel(), "device");
			
			// Initialize the router:
			this.getRouter().initialize();
		}
	});
});