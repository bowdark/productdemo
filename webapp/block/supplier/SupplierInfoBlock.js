sap.ui.define(["sap/uxap/BlockBase"], function(BlockBase) {
	"use strict";
	return BlockBase.extend("com.bowdark.products.block.supplier.SupplierInfoBlock", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "com.bowdark.products.block.supplier.SupplierInfo",
					type: "XML"
				},
				Expanded: {
					viewName: "com.bowdark.products.block.supplier.SupplierInfo",
					type: "XML"
				}
			}
		}	
	});
}, true);