sap.ui.define(["sap/uxap/BlockBase"], function(BlockBase) {
	"use strict";
	return BlockBase.extend("com.bowdark.products.block.product.ProductInfoBlock", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "com.bowdark.products.block.product.ProductInfo",
					type: "XML"
				},
				Expanded: {
					viewName: "com.bowdark.products.block.product.ProductInfo",
					type: "XML"
				}
			}
		}	
	});
}, true);